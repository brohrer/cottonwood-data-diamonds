import numpy as np

n_samples = 100
data_filename = "diamonds.csv"
sample_filename = "diamonds_sample.csv"

with open(data_filename, "rt") as f:
    data_lines = f.readlines()

    print(data_lines)
i_keepers = np.random.choice(
    len(data_lines) - 1,
    size=n_samples + 1,
    replace=False)

with open(sample_filename, "wt") as f:
    f.write(data_lines[0])
    for i_line in i_keepers:
        f.write(data_lines[i_line])
