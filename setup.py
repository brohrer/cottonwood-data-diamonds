import setuptools as st

st.setup(
    name='cottonwood_data_diamonds',
    version='1',
    description='Mirror of diamonds data set in a Cottonwood block',
    url='https://gitlab.com/brohrer/cottonwood-data-diamonds',
    download_url='https://gitlab.com/brohrer/cottonwood-data-diamonds/-/archive/main/cottonwood-data-diamonds-main.zip',
    author='Brandon Rohrer',
    author_email='brohrer@gmail.com',
    license='MIT',
    install_requires=["numpy"],
    package_data={
        "": [
            "diamonds.csv",
            "diamonds_sample.csv",
        ],
    },
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)
