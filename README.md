# Diamond prices data set

A diamond price data set, taken from the [ggplot2](https://ggplot2.tidyverse.org/) package in R.

I posted it here under the GNU GPL 2.0 License, the license under which ggplot2 is offered. 

Also available [on Kaggle](https://www.kaggle.com/shivam2503/diamonds).

Heads up, this data set is quite a few years old now. Don't try to use it for any actual diamond purchase planning.

## diamonds.csv

The raw data.

columns:
* 0: ID number
* 1: weight in carats
* 2: cut, one of Fair, Good, Very Good, Premium, Ideal
* 3: color, one of D (clear/white) to J (yellow)
* 4: clarity, one of I1, SI2, SI1, VS2, VS1, VVS2, VVS1, IF
* 5: depth, measured from the culet to the table, divided by its average girdle diameter
* 6: table, width of the diamond's table expressed as a percentage of its average diameter
* 7: price in $USD
* 8: x, length in mm
* 9: y, width in mm
* 10: z, depth in mm

## diamonds_sample.csv

A small sample of the original data set. Good for testing.

## create_sample.py  

The script to create `diamonds_sample.py` from `diamonds.py`.

## diamonds_block.py

A wrapper for the data to make it easy to pull this data into [the Cottonwood machine learning framework](https://e2eml.school/cottonwood). To see it in action, check out [this case study that uses it](https://gitlab.com/brohrer/study-knn-diamonds).
